<?php

use PHPUnit\Framework\TestCase;
use Levi\Dungeon\Characters\Human;

class CharacterTests extends TestCase
{
    protected $character;

    public function setUp()
    {
        $this->character = new Human('levi');
    }

    public function testGetName()
    {
        $this->assertEquals('levi', $this->character->name());
    }

    public function testGetLevel()
    {
        $this->assertEquals(1, $this->character->level());
    }

    public function testGetHealth()
    {
        $this->assertEquals(100, $this->character->health());
    }

    // public function testGetEquipment()
    // {
    //     // TODO: This test will most likely change
    //     $this->assertEquals([], $this->character->equipment());
    // }

    public function testSetCoordinate()
    {
        $x = 5;
        $y = 4;
        $c = new Levi\Dungeon\Support\Coordinate($x, $y);
        $this->character->setCoordinate($c);
        $this->assertEquals($x, $this->character->coordinate()->x());
        $this->assertEquals($y, $this->character->coordinate()->y());
    }

    public function testGetId()
    {
        $this->assertTrue(is_string($this->character->id()));
    }
}
