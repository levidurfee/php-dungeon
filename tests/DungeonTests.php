<?php

use PHPUnit\Framework\TestCase;
use Levi\Dungeon\Characters\Human;
use Levi\Dungeon\Dungeons\Harmstead;
use Levi\Dungeon\Support\Coordinate;

class DungeonTests extends TestCase
{
    public function test_isOpen_method()
    {
        $human = new Human('test dummy');
        $dungeon = new Harmstead($human);

        $this->assertTrue($dungeon->isOpen(new Coordinate(1, 1)));
        $this->assertFalse($dungeon->isOpen(new Coordinate(0, 0)));
    }

    public function test_dungeon_has_id()
    {
        $human = new Human('test dummy');
        $dungeon = new Harmstead($human);

        $this->assertTrue(is_string($dungeon->id()));
    }
}
