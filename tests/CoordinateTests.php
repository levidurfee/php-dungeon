<?php

use PHPUnit\Framework\TestCase;
use Levi\Dungeon\Support\Coordinate;

class CoordinateTests extends TestCase
{
    protected $x;
    protected $y;
    protected $c;

    public function setUp()
    {
        $this->c = new Coordinate($this->x = 1, $this->y = 1);
    }

    public function testCreatingCoordinateClass()
    {
        $this->assertInstanceOf(Coordinate::class, $this->c);
    }

    public function testUp()
    {
        $spaces = 1;
        $c = $this->c->up();
        $this->assertEquals($this->x-$spaces, $c->x());
        $this->assertEquals($this->y, $c->y());
    }

    public function testUpMultipleSpaces()
    {
        $spaces = 5;
        $c = $this->c->up($spaces);
        $this->assertEquals($this->x-$spaces, $c->x());
        $this->assertEquals($this->y, $c->y());
    }

    public function testDown()
    {
        $spaces = 1;
        $c = $this->c->down();
        $this->assertEquals($this->x+$spaces, $c->x());
        $this->assertEquals($this->y, $c->y());
    }

    public function testDownMultipleSpaces()
    {
        $spaces = 5;
        $c = $this->c->down($spaces);
        $this->assertEquals($this->x+$spaces, $c->x());
        $this->assertEquals($this->y, $c->y());
    }

    public function testLeft()
    {
        $spaces = 1;
        $c = $this->c->left($spaces);
        $this->assertEquals($this->x, $c->x());
        $this->assertEquals($this->y+$spaces, $c->y());
    }

    public function testLeftMultipleSpaces()
    {

        $spaces = 5;
        $c = $this->c->left($spaces);
        $this->assertEquals($this->x, $c->x());
        $this->assertEquals($this->y+$spaces, $c->y());
    }

    public function testRight()
    {
        $spaces = 1;
        $c = $this->c->right($spaces);
        $this->assertEquals($this->x, $c->x());
        $this->assertEquals($this->y-$spaces, $c->y());
    }


    public function testRightMultipleSpaces()
    {
        $spaces = 5;
        $c = $this->c->right($spaces);
        $this->assertEquals($this->x, $c->x());
        $this->assertEquals($this->y-$spaces, $c->y());
    }

    public function testGetX()
    {
        $this->assertEquals($this->x, $this->c->x());
    }


    public function testGetY()
    {
        $this->assertEquals($this->y, $this->c->y());
    }
}
