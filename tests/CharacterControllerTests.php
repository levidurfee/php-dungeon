<?php

use PHPUnit\Framework\TestCase;

use Levi\Dungeon\Characters\Human;
use Levi\Dungeon\Controllers\CharacterController;


class CharacterControllerTests extends TestCase
{
    protected $human;
    protected $controller;

    public function setUp()
    {
        $this->human = new Human('lil human');
        $dungeon = new Levi\Dungeon\Dungeons\Harmstead($this->human);
        $this->controller = new CharacterController($this->human, $dungeon);
    }

    public function testDown()
    {
        $this->controller->down();
        $this->assertEquals(3, $this->human->coordinate()->x());
    }

    public function testUp()
    {
        $this->controller->up();
        $this->assertEquals(1, $this->human->coordinate()->x());
    }

    public function testLeft()
    {
        $this->controller->left();
        $this->assertEquals(3, $this->human->coordinate()->y());
    }

    public function testRight()
    {
        $this->controller->right();
        $this->assertEquals(1, $this->human->coordinate()->y());
    }
}
