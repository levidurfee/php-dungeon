<?php

require 'vendor/autoload.php';

$bag = new Levi\Dungeon\Items\Bag;
$bagContainer = new Levi\Dungeon\Containers\BagContainer($bag);

$human[0] = new Levi\Dungeon\Characters\Human('levi', 40, 100, $bagContainer);
$human[1] = new Levi\Dungeon\Characters\Human('tadd');
$dungeon = new Levi\Dungeon\Dungeons\Harmstead(...$human);
$levi = new Levi\Dungeon\Controllers\CharacterController($human[0], $dungeon);
$tadd = new Levi\Dungeon\Controllers\CharacterController($human[1], $dungeon);

// $levi->up();
// dump($human[0]);
// $tadd->down()->down();
// dump($human[1]);

// dump($dungeon);

$healingPotion = new Levi\Dungeon\Items\Equipment\Potions\HealingPotion;
$human[0]->bags()->get($bag->id())->add($healingPotion);
dump($human[0]);
