<?php

namespace Levi\Dungeon\Items;

use Levi\Dungeon\Contracts\Helpers\IdentifiableInterface;
use Levi\Dungeon\Contracts\Items\BagInterface;
use Levi\Dungeon\Support\Traits\Identifiable;
use Levi\Dungeon\Contracts\EquipmentInterface;

// Characters can only carry a weapon, and bags. Bags can carry other equipment
class Bag implements BagInterface, IdentifiableInterface
{
    use Identifiable;

    protected $max;

    protected $occupies = 1;

    protected $equipment = [];

    public function __construct(EquipmentInterface $equipment = null, int $max = 1)
    {
        if ($equipment) {
            $this->equipment[$equipment->id()] = $equipment;
        }
        $this->max = $max;

        $this->setId();
    }

    public function add(EquipmentInterface $equipment): bool
    {
        if ($this->size() < $this->max) {
            $this->equipment[$equipment->id()] = $equipment;

            return true;
        }

        return false;
    }

    public function getAll()
    {
        return $this->equipment;
    }

    public function get(string $id)
    {
        if (isset($this->equipment[$id])) {
            return $this->equipment[$id];
        }
    }

    public function size()
    {
        return count($this->equipment);
    }
}
