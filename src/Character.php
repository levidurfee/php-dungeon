<?php

namespace Levi\Dungeon;

use Levi\Dungeon\Support\Traits\Identifiable;
use Levi\Dungeon\Contracts\CharacterInterface;
use Levi\Dungeon\Contracts\Helpers\IdentifiableInterface;
use Levi\Dungeon\Contracts\Support\CoordinateInterface;
use Levi\Dungeon\Contracts\Items\WeaponInterface;
use Levi\Dungeon\Contracts\Containers\BagContainerInterface;

abstract class Character implements CharacterInterface, IdentifiableInterface
{
    use Identifiable;

    protected $coordinate;

    protected $name;

    protected $level;

    protected $health;

    protected $bags;

    protected $weapon;

    public function __construct(string $name, int $level = 1, int $health = 100, BagContainerInterface $bags = null, WeaponInterface $weapon = null)
    {
        $this->name = $name;
        $this->level = $level;
        $this->health = $health;
        $this->bags = $bags;
        $this->weapon = $weapon;

        $this->setId();
    }

    public function name()
    {
        return $this->name;
    }

    public function level()
    {
        return $this->level;
    }

    public function health()
    {
        return $this->health;
    }

    public function bags()
    {
        return $this->bags;
    }

    public function setCoordinate(CoordinateInterface $coordinate)
    {
        $this->coordinate = $coordinate;
    }

    public function coordinate(): CoordinateInterface
    {
        return $this->coordinate;
    }

    public function weapon()
    {
        return $this->weapon;
    }
}
