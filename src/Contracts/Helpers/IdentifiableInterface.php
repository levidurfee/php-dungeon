<?php

namespace Levi\Dungeon\Contracts\Helpers;

interface IdentifiableInterface
{
    public function id();
}
