<?php

namespace Levi\Dungeon\Contracts;

use Levi\Dungeon\Contracts\Support\HasCoordinates;

interface CharacterInterface extends HasCoordinates
{
    public function id();
    
    public function name();

    public function level();

    public function health();

    public function bags();

    public function weapon();
}
