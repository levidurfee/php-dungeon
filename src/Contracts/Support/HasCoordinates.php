<?php

namespace Levi\Dungeon\Contracts\Support;

use Levi\Dungeon\Contracts\Support\CoordinateInterface;

interface HasCoordinates
{
    public function setCoordinate(CoordinateInterface $coordinate);

    public function coordinate(): CoordinateInterface;
}
