<?php

namespace Levi\Dungeon\Contracts\Support;

interface CoordinateInterface
{
    public function x(): int;

    public function y(): int;

    public function up(int $spaces): CoordinateInterface;

    public function down(int $spaces): CoordinateInterface;

    public function left(int $spaces): CoordinateInterface;

    public function right(int $spaces): CoordinateInterface;
}
