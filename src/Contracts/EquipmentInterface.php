<?php

namespace Levi\Dungeon\Contracts;

interface EquipmentInterface
{
    public function occupies();
}
