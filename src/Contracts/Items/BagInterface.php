<?php

namespace Levi\Dungeon\Contracts\Items;

use Levi\Dungeon\Contracts\EquipmentInterface;

interface BagInterface
{
    public function add(EquipmentInterface $equipment): bool;

    public function getAll();

    public function get(string $id);

    public function size();
}
