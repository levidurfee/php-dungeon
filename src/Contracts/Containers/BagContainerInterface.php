<?php

namespace Levi\Dungeon\Contracts\Containers;

use Levi\Dungeon\Contracts\Items\BagInterface;

interface BagContainerInterface
{
    public function add(BagInterface $bag): bool;

    public function getAll();

    public function get(string $id);

    public function size();
}
