<?php

namespace Levi\Dungeon\Controllers;

use Levi\Dungeon\Contracts\ControllerInterface;
use Levi\Dungeon\Contracts\CharacterInterface;
use Levi\Dungeon\Contracts\DungeonInterface;

class CharacterController implements ControllerInterface
{
    private $character;

    private $dungeon;

    public function __construct(CharacterInterface $character, DungeonInterface $dungeon)
    {
        $this->character = $character;
        $this->dungeon = $dungeon;
    }

    public function up(): ControllerInterface
    {
        if ($this->dungeon->isOpen($this->character->coordinate()->up())) {
            $this->character->setCoordinate($this->character->coordinate()->up());
        }

        return $this;
    }

    public function down(): ControllerInterface
    {
        if ($this->dungeon->isOpen($this->character->coordinate()->down())) {
            $this->character->setCoordinate($this->character->coordinate()->down());
        }
        
        return $this;
    }

    public function left(): ControllerInterface
    {
        if ($this->dungeon->isOpen($this->character->coordinate()->left())) {
            $this->character->setCoordinate($this->character->coordinate()->left());
        }

        return $this;
    }

    public function right(): ControllerInterface
    {
        if ($this->dungeon->isOpen($this->character->coordinate()->right())) {
            $this->character->setCoordinate($this->character->coordinate()->right());
        }

        return $this;
    }
}
