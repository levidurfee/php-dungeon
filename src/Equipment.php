<?php

namespace Levi\Dungeon;

use Levi\Dungeon\Contracts\EquipmentInterface;
use Levi\Dungeon\Contracts\Helpers\IdentifiableInterface;
use Levi\Dungeon\Support\Traits\Identifiable;

abstract class Equipment implements EquipmentInterface, IdentifiableInterface
{
    use Identifiable;
    
    protected $occupies = 1;

    public function __construct()
    {
        $this->setId();
    }

    public function occupies()
    {
        return $this->occupies;
    }
}
