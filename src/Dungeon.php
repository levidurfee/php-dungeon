<?php

namespace Levi\Dungeon;

use Levi\Dungeon\Support\Traits\Identifiable;
use Levi\Dungeon\Contracts\DungeonInterface;
use Levi\Dungeon\Contracts\Helpers\IdentifiableInterface;
use Levi\Dungeon\Contracts\CharacterInterface;
use Levi\Dungeon\Support\Coordinate;
use Levi\Dungeon\Contracts\Support\CoordinateInterface;

abstract class Dungeon implements DungeonInterface, IdentifiableInterface
{
    use Identifiable;

    protected $barriers = [];

    protected $characters = [];

    protected $startingX;
    protected $startingY;

    public function __construct(CharacterInterface ...$characters)
    {
        foreach ($characters as $character) {
            $this->characters[$character->id()] = $character;
            $character->setCoordinate(new Coordinate($this->startingX, $this->startingY));
        }

        $this->setId();
    }

    public function isOpen(CoordinateInterface $coordinate): bool
    {
        if ($this->barriers[$coordinate->x()][$coordinate->y()] == 0) {
            return true;
        }

        return false;
    }
}
