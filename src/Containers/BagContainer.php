<?php

namespace Levi\Dungeon\Containers;

use Levi\Dungeon\Contracts\Items\BagInterface;
use Levi\Dungeon\Contracts\Containers\BagContainerInterface;

class BagContainer implements BagContainerInterface
{
    protected $max;

    protected $bags = [];

    public function __construct(BagInterface $bag, int $max = 1)
    {
        $this->bags[$bag->id()] = $bag;
        $this->max = $max;
    }

    public function add(BagInterface $bag): bool
    {
        if ($this->size() < $this->max) {
            $this->bags[$bag->id()] = $bag;

            return true;
        }

        return false;
    }

    public function getAll()
    {
        return $this->bags;
    }

    public function get(string $id)
    {
        if (isset($this->bags[$id])) {
            return $this->bags[$id];
        }
    }

    public function size()
    {
        return count($this->bags);
    }
}
