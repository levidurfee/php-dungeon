<?php

namespace Levi\Dungeon\Support;

use Levi\Dungeon\Contracts\Support\CoordinateInterface;

/**
 * Coordinate support class for simplifying x, y coordinates.
 */
class Coordinate implements CoordinateInterface
{
    /**
     * X
     *
     * @var int
     */
    private $x;

    /**
     * Y
     *
     * @var int
     */
    private $y;

    /**
     *
     * @param int $x X
     * @param int $y Y
     */
    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * X Accessor
     *
     * @return int
     */
    public function x(): int
    {
        return $this->x;
    }

    /**
     * Y Accessor
     *
     * @return int
     */
    public function y(): int
    {
        return $this->y;
    }

    // May need to check for negative numbers and throw an exception

    /**
     * Get the x,y values above the current coordinates.
     *
     * @param int $spaces Optional
     *
     * @return CoordinateInterface
     */
    public function up(int $spaces = 1): CoordinateInterface
    {
        return new Coordinate($this->x - $spaces, $this->y);
    }

    /**
     * Get the x,y values below the current coordinates.
     *
     * @param int $spaces Optional
     *
     * @return CoordinateInterface
     */
    public function down(int $spaces = 1): CoordinateInterface
    {
        return new Coordinate($this->x + $spaces, $this->y);
    }

    /**
     * Get the x,y values left the current coordinates.
     *
     * @param int $spaces Optional
     *
     * @return CoordinateInterface
     */
    public function left(int $spaces = 1): CoordinateInterface
    {
        return new Coordinate($this->x, $this->y + $spaces);
    }

    /**
     * Get the x,y values right the current coordinates.
     *
     * @param int $spaces Optional
     *
     * @return CoordinateInterface
     */
    public function right(int $spaces = 1): CoordinateInterface
    {
        return new Coordinate($this->x, $this->y - $spaces);
    }
}
