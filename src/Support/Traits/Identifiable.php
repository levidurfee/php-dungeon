<?php

namespace Levi\Dungeon\Support\Traits;

trait Identifiable
{
    protected $id;

    public function id()
    {
        return $this->id;
    }

    public function setId()
    {
        $this->id = uniqid();
    }
}
