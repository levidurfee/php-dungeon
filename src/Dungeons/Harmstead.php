<?php

namespace Levi\Dungeon\Dungeons;

use Levi\Dungeon\Dungeon;

class Harmstead extends Dungeon
{
    protected $startingX = 2;
    protected $startingY = 2;

    protected $barriers = [
        [1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1],
    ];

    protected $objects = [
        ['4,4' => 'New Sword'],
        /*
        // We could do something like this
        ['4,4' => function() {
            return new LongSword;
        }]
        */
    ];

    protected $portals = []; // Could lead to another Dungeon
}
